import java.io.*;
import java.util.Random;
import java.lang.Math;

class Blackjack {

    public static void main(String args[]) throws IOException {

	BufferedReader stdin = new BufferedReader
	    (new InputStreamReader(System.in));
	Random generator = new Random(); // used to generate random cards.

	int score = 0, computerscore = 0; // keeps track of scores
	boolean hit = true; // true while player wants to hit.
	int numcards = 0; // keeps track of the number of cards the user has.

	System.out.println("Welcome to Blackjack against the computer.");

	// Loop runs till user doesn't want another card or has busted.
	while (hit) {

	    int singlecard = 0; // Temp. var. to make sure following loop runs
	                        // draw only one card after initial two cards are given.

	    // Gives player 2 cards initially, then 1 every time it's run thereafter.
	    while (numcards < 2 || singlecard < 1) {

		int rand = Math.abs(generator.nextInt())%13; // next card.

		// Handles case of player getting an ace. Let's user choose value.
		if (rand == 0) {
		    System.out.println("You have gotten an ace, would you like it to count as 11 or 1.");
		    int acescore = Integer.parseInt(stdin.readLine());

		    if (acescore != 11 && acescore != 1) {
			System.out.println("Sorry, that is not a valid point value, your ace has been assigned 11 points.");
			score = score + 11;
		    }
		    else
			score = score + acescore;

		}

		// Scores other cases.
		else if (rand < 10) {
		    System.out.println("Your card value = " + (rand + 1));
		    score = score + rand + 1;
		}
		else {
		    System.out.println("Your card value = 10");
		    score = score + 10;
		}

		// Increment appropriate counters.
		numcards++;
		singlecard++;
	    }

	    // Prints out player's current score and asks if he/she wants
	    // another hit.
	    System.out.println("So, your current score is = " + score);
	    if (score <= 21) {
		System.out.println("Would you like to hit again?");
		char ans = (stdin.readLine()).charAt(0);
		if (ans != 'y' && ans != 'Y')
		    hit = false;
	    }
	    else
		hit = false;
	}

	// Prints out player's final score
	System.out.println("So, your final score = " + score);

	// Executes computers turn. It hits till 17.
	while (computerscore < 17) {

	    int rand = Math.abs(generator.nextInt())%13; //next random card.
	    int cardvalue; // used to store current card's point value.

	    // Picks score of an ace, so as not to bust.
	    if (rand == 0) {
		if (computerscore < 11)
		    cardvalue = 11;
		else
		    cardvalue = 1;
	    }
	    else if (rand < 10)
		cardvalue = rand + 1;
	    else
		cardvalue = 10;

	    // Prints out computer's score after every card.
	    computerscore = computerscore + cardvalue;
	    System.out.println("Computer card value = " + cardvalue);
	}

	// Prints out computer's final score.
	System.out.println("The computer score = " + computerscore);

	// Prints out winner to the screen.
	if (score > 21 && computerscore > 21)
        System.out.println("You and the computer have both busted. No winners today.");
	else if (score > 21)
        System.out.println("You have busted. The computer wins!");
	else if (computerscore > 21)
        System.out.println("The computer has busted. You win!");
	else {
	    if (score > computerscore)
            System.out.println("You win!!!");
	    else if (score < computerscore)
            System.out.println("The computer wins!");
	    else
		System.out.println("There is a tie.");
	}
    }

}
