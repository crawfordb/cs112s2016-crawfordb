//import java.util.LinkedList;
//import java.util.Iterator;

class LinkedListTester {

	public static void main(String args[]) {

		LL myList = new LL();

		// add stuff to the list
		myList.add(12);
		myList.add(99);
		myList.add(37);
		//myList.add("Hey look it's a string");

		// retrieve something from the list
		//System.out.println(myList.get(0));
		printListContents(myList);
		
		/*
		System.out.println(myList.get(-4));
		System.out.println(myList.get(117));
		*/

		int actualSize = myList.size();
		for (int i = 0; i < actualSize; i++) {
			myList.remove(0);
		} //for

/*
		don't do this
   		myList.remove(0);
		myList.remove(1);
		myList.remove(2);
*/
		printListContents(myList);
		myList.add(12);
		myList.add(99);
		myList.add(37);

		myList.add(1, 97);
		printListContents(myList);

/*
		myList.add(1, 99);
		printListContents(myList);
*/
/*
		LinkedList<Double> myList2 = new LinkedList<Double>();
		int breakattempt = 7;
		myList2.add(123.456);
		myList2.add(412.814);
		//myList2.add("This should break the program");
		myList2.add((double)breakattempt);

		printListContents(myList2);*/

	} //main

	public static void printListContents(LL ll) {
		for (int i = 0; i < ll.size(); i++) {
			System.out.println(ll.get(i));
		} //for

/*		Iterator iter = ll.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		} //while*/
		System.out.println();
	} //printListContents

} //LinkedListTester
