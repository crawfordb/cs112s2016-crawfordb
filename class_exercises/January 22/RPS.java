import java.util.Scanner;
import java.util.Random;

//import java.util.*;

public class RPS {

	static int userScore = 0, computerScore = 0, tiesScore = 0;

    public static void main(String[] args) {

        do {

            playRound();

        } while (playAgain()); // do-while

        System.out.println("Hey thanks for playing!");

    } // main

    public static int roundWinner(String u, String c) {
        //0 = user wins
        //1 = computer wins
        //2 = tie

        if (u.equals(c)) {
			return 2;

		} else if (u.equals("ROCK") && c.equals("PAPER")) {
			return 1;

		} else if (u.equals("PAPER") && c.equals("ROCK")) {
			return 0;

		} else if (u.equals("ROCK") && c.equals("SCISSORS")) {
			return 0;

		} else if (u.equals("SCISSORS") && c.equals("ROCK")) {
			return 1;

		} else if (u.equals("PAPER") && c.equals("SCISSORS")) {
			return 1;

		} else /*(u.equals("SCISSORS") && c.equals("PAPER"))*/ {
			return 0;

		} //if-else

    }// roundWinner

    public static String generateComputerChoice() {
        //0 = ROCK
        //1 = PAPER
        //3= SCISSORS

        Random rand = new Random();
        int computerChoice;

        computerChoice = rand.nextInt(3);

        if(computerChoice == 0) {
            System.out.println("Computer picks ROCK");
            return "ROCK";
        }else if (computerChoice == 1) {
            System.out.println("Computer picks PAPER");
            return "PAPER";
        } else if (computerChoice == 2) {
            System.out.println("Computer picks SCISSORS");
            return "SCISSORS";
        } //if-else

        System.out.println("Something very bad has happened, returning ROCK by default");
        return "ROCK";
    } //generateComputerChoice

    public static String getUserInput() {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter your choice (ROCK, PAPER, SCISSORS): ");
        String userChoice;
        userChoice = scan.next().toUpperCase();

        return userChoice;
    } //getUserInput

    public static boolean playAgain() {
		Scanner scan = new Scanner(System.in);

		System.out.println("Do you want to play again? (YES, NO)");
		String userInput = scan.next().toUpperCase();

		if (userInput.equals("YES")) {
			return true;
		} else if (userInput.equals("NO")) {
        	return false;
		} else {
			System.out.println("Hey, you didn't answer YES or NO");
			return playAgain();
		}//if-else
    } //playAgain

    public static void playRound() {
        String userChoice;
        String computerChoice;
        //int userScore = 0, computerScore = 0, tiesScore = 0;
        int winner;

        //Get a command from the user.
        userChoice = getUserInput();

        //Generate a computer choice.
        computerChoice = generateComputerChoice();

        //Pick a winner.
        winner = roundWinner(userChoice, computerChoice);

        //Increment the appropriate score.
        switch (winner) {

            case 0:
                userScore++;
                break;

            case 1:
                computerScore++;
                break;

            case 2:
                tiesScore++;
                break;

            default:
                System.out.println("Something very bad has happened.");
                break;


        } //switch

        //Display the scores.
        printScores(userScore, computerScore, tiesScore);

    } // playRound

    public static void printScores(int u, int c, int t) {

		System.out.println("The user score is " + u);
		System.out.println("The computer score is " + c);
		System.out.println("THere have been " + t + " ties.");

    } // printScores

} //RPS (close)
