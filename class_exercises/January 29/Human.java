import java.util.Scanner;

public class Human {

	private int userScore;

	public Human() {
		userScore = 0;
	} //Human (constructor)

	public int getUserScore() {
		return userScore;
	} //getUserScore

	public static String getUserInput() {
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter your choice (ROCK, PAPER, SCISSORS): ");
		String userChoice;

		userChoice = scan.next().toUpperCase();

		return userChoice;
	} //getUserInput

	public void incrementScore() {
		userScore++;
	} //incrementScore

} //Human (class)
