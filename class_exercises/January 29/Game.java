import java.util.Scanner;

public class Game {

	private static int tiesScore;
	private static Human player;
	private static Computer cpu;

	public static void main(String[] args) {

		player = new Human();
		cpu = new Computer();
		
		do {

			playRound();


		} while (playAgain()); // do-while

		System.out.println("Hey thanks for playing");

	} // main

	public static int roundWinner(String u, String c) {
		// 0 = user wins
		// 1 = computer wins
		// 2 = there was a tie

		if (u.equals(c)) {
			return 2;

		} else if (u.equals("ROCK") && c.equals("PAPER")) {
			return 1;

		} else if (u.equals("PAPER") && c.equals("ROCK")) {
			return 0;

		} else if (u.equals("ROCK") && c.equals("SCISSORS")) {
			return 0;

		} else if (u.equals("SCISSORS") && c.equals("ROCK")) {
			return 1;

		} else if (u.equals("PAPER") && c.equals("SCISSORS")) {
			return 1;

		} else /*(u.equals("SCISSORS") && c.equals("PAPER"))*/ {
			return 0;

		} //if-else

	} //roundWinner

	public static boolean playAgain() {
		Scanner scan = new Scanner(System.in);

		System.out.println("Do you want to play again? (YES, NO)");
		String userInput = scan.next().toUpperCase();
	int userScore = 0, computerScore = 0, tiesScore = 0;
		if (userInput.equals("YES")) {
			return true;
		} else if (userInput.equals("NO")) {
			return false;
		} else {
			System.out.println("Hey, you didn't answer YES or NO");
			return playAgain();
		} //if-else
	} //playAgain

	public static void playRound() {
		String userChoice;
		String computerChoice;
		//int userScore, computerScore, tiesScore;
		int winner;

		// Get a command from the user
		userChoice = player.getUserInput();

		// Generate a computer choice
		computerChoice = cpu.generateComputerChoice();

		
		// Pick a winner
		winner = roundWinner(userChoice, computerChoice);

		// Increment the appropriate score
		switch (winner) {

			case 0:
				player.incrementScore();
				break;

			case 1:
				cpu.incrementScore();
				break;

			case 2:
				tiesScore++;
				break;

			default:
				System.out.println("Something very bad has happened.");
				break;

		} //switch

		// Display the scores
		printScores(player.getUserScore(), cpu.getComputerScore(), tiesScore);
		

	} //playRound

	public static void printScores(int u, int c, int t) {
		System.out.print("The user score is ");
		System.out.println(u);
		System.out.println("The computer score is " + c);
		System.out.println("There have been " + t + " ties");
	} //printScores


} //Game (class)
