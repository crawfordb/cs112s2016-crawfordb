import java.util.Scanner;

public class RecursiveFibonacci {

	public static void main(String args[]) {

		System.out.println("Enter a number!");

		Scanner scan = new Scanner(System.in);
		int target = scan.nextInt();
		long output;

		long startTime = System.nanoTime();
		output = fib(target);
		long endTime = System.nanoTime();
		long timeElapsed = endTime - startTime;


		System.out.println("Fibonacci number " + target + " is " + output);
		System.out.println("Runtime was " + timeElapsed + " nanoseconds");
		System.out.println("That means " + timeElapsed/1_000_000_000.0 + " seconds");


	} //main

	public static long fib(int n) {

		// Base case
		if (n == 0) {
			return 0;
		} else if (n == 1) {
			return 1;
		
		// Recursive case
		} else {
			return fib(n-1) + fib(n-2);
		} //if-else	

	} //fib

} //RecursiveFactorial (class)
