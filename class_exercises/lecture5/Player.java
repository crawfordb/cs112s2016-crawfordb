public class Player {

	private int score;

	public Player() {
		score = 0;
	} //Player (constructor)

	public Player(int startingScore) {
		score = startingScore;
	} //Player (constructor)
		
	public int getScore() {
		return score;
	} //getScore

	public void incrementScore() {
		score++;
	} //incrementScore

} //Player (class)
