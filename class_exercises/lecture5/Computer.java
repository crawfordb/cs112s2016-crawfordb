import java.util.Random;

public class Computer extends Player {

	//private int computerScore;

	public Computer() {
		//computerScore = 0;
		super(0);
	} //Computer (constructor)
/*
	public int getComputerScore() {
		return computerScore;
	} //getComputerScore
*/
	public String generateComputerChoice() {
		// 0 = ROCK
		// 1 = PAPER
		// 2 = SCISSORS

		Random rand = new Random();
		int computerChoice;

		computerChoice = rand.nextInt(3);

		if (computerChoice == 0) {
			System.out.println("Computer picks ROCK");
			return "ROCK";
		} else if (computerChoice == 1) {
			System.out.println("Computer picks PAPER");
			return "PAPER";
		} else if (computerChoice == 2) {
			System.out.println("Computer picks SCISSORS");
			return "SCISSORS";
		} //if-else

		System.out.println("Something very bad has happened, returning ROCK by default");
		return "ROCK";

	} //generateComputerChoice  

	public void incrementScore() {
		//computerScore++;
		//score = score + 2;
		super.incrementScore();
		super.incrementScore();
	} //incrementScore

} //Computer (class)
