import java.util.Stack;

class Parenthesis {

	public static void main(String args[]) {
		char input[] = args[0].toCharArray();
		
		if (matcher(input)) {
			System.out.println("We're good!");
		} else {
			System.out.println("Bad things have happened to our string");
		} //if-else

	} //main

	public static boolean matcher(char[] c) {

		Stack<Character> myStack = new Stack<Character>();

		for (int i = 0; i < c.length; i++) {
			
			if (c[i] == '(' || c[i] == '[' || c[i] == '{') {
				myStack.push(c[i]);

			} else if (!myStack.empty()) {

				if (c[i] == ')') {
					char temp = myStack.pop();
					if (temp != '(') {
						return false;
					} //if
	
				} else if (c[i] == ']') {
					char temp = myStack.pop();
					if (temp != '[') {
						return false;
					} //if
	
				} else if (c[i] == '}') {
					char temp = myStack.pop();
					if (temp != '{') {
						return false;
					} //if

				} //if-else

			} else if (myStack.empty() && (c[i] == ')' || c[i] == ']' || c[i] == '}')) {
				return false;

			} //if-else

		} //for

		return myStack.empty();
	} //matcher

} //Parenthesis (class)
