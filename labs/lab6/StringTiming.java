import java.util.Scanner;

public class StringTiming {

	public static void main(String[] args) {

		long startTime, endTime, elapsed;
		long startTime2, endTime2, elapsed2;
		int N;

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter a number for the size of the loop (1000, 10000, 50000, 100000): ");
		N = scan.nextInt();

		String myString = new String();

		startTime = System.nanoTime();

		for (int i = 0; i < N; i++) {
			myString = myString + "a";
		} // for
		
		endTime = System.nanoTime();

		elapsed = endTime - startTime;

		System.out.println("Time in nanoseconds for String: "+elapsed);

		// StringBuilder code
		StringBuilder mySB = new StringBuilder();
		
		startTime2 = System.nanoTime();

		for (int i = 0; i < N; i++) {
			mySB.append("a");
		} // for
		
		endTime2 = System.nanoTime();

		elapsed2 = endTime2 - startTime2;

		System.out.println("Time in nanoseconds for StringBuilder: "+elapsed2);

	} // main
} // StringTiming (class)
