import java.util.Scanner;
import java.util.LinkedList;

public class LinkedListTiming {

	public static void main(String[] args) {

		long startTime, endTime, elapsed;
		long startTime2, endTime2, elapsed2;
		long startTime3, endTime3, elapsed3;
		long startTime4, endTime4, elapsed4;
		long startTime5, endTime5, elapsed5;
		long startTime6, endTime6, elapsed6;
		int N;

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter a number for the size (1000, 10000, 50000, 100000): ");
		N = scan.nextInt();

		// Adding array
		int[] myArray = new int[N];

		startTime = System.nanoTime();

		for(int i = 0; i < N; i++) {
			myArray[i] = i;
		} // for

		endTime = System.nanoTime();

		elapsed = endTime - startTime;

		System.out.println("Time in nanoseconds for adding Array: "+elapsed);

		// Adding LinkedList
		LinkedList<Integer> myList = new LinkedList<Integer>();

		startTime2 = System.nanoTime();

		for(int i = 0; i < N; i++) {
			myList.add(i);
		} // for

		endTime2 = System.nanoTime();

		elapsed2 = endTime2 - startTime2;

		System.out.println("Time in nanoseconds for LinkedList: "+elapsed2);

		// Adding LL
		LL mySList = new LL();

		startTime3 = System.nanoTime();

		for(int i = 0; i < N; i++) {
			mySList.add(i);
		} // for

		endTime3 = System.nanoTime();

		elapsed3 = endTime3 - startTime3;

		System.out.println("Time in nanoseconds for LL: "+elapsed3);

		// Retrieving array
		startTime4 = System.nanoTime();

		for(int i = 0; i < N; i++) {
			int something = myArray[i];
		} // for

		endTime4 = System.nanoTime();

		elapsed4 = endTime4 - startTime4;

		System.out.println("Time in nanoseconds for retrieving Array: "+elapsed4);

		// Retrieving LinkedList
		startTime5 = System.nanoTime();

		for(int i = 0; i < N; i++) {
			int variable = myList.get(i);
		} // for

		endTime5 = System.nanoTime();

		elapsed5 = endTime5 - startTime5;

		System.out.println("Time in nanoseconds for retrieving LinkedList: "+elapsed5);

		// Retrieving LL
		startTime6 = System.nanoTime();

		for(int i = 0; i < N; i++) {
			int somethingElse = mySList.get(i);
		} // for

		endTime6 = System.nanoTime();

		elapsed6 = endTime6 - startTime6;

		System.out.println("Time in nanoseconds for retrieving LL: "+elapsed6);

	} // main
} // LinkedListTiming (class)
