public class Rect extends Quad {
	
	// Rect constructor takes the variables from the Quad class and sets them. 
    Rect(double width, double height) {
        width1 = width;
        height1 = height;
    } // Rect (constructor)

    // Calculates the area of a rectangle.
    public void calculateArea() {
        area = getWidth() * getHeight();
    } // calculateArea

    // Calculates the perimeter of a rectangle.
    public void calculatePerimeter() {
        perimeter = 2 * (getWidth() + getHeight());
    } // calculatePrimeter

} // Rect (class)
