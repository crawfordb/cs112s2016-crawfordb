public class Round extends Shape {

    private double pi = 3.1415926535;

    // Accessor to allow the Circle and Oval classes to use the variable pi.
    public double getPi() {
        return pi;
    } // getPi

} // Round (class)
