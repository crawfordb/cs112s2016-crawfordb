public class Shape {

    protected double area = 0;
    protected double perimeter = 0;

	// Sets the area.
	public void setArea(int newArea) {
		area = newArea;
	} // setArea

	// Sets the perimeter.
	public void setPerimeter(int newPerimeter) {
		perimeter = newPerimeter;
	} // setPerimeter

    // Accessor to allow the child classes to use the variable area.
    public double getArea() {
		return area;
	} // getArea

    // Accessor to allow the child classes to use variable perimeter.
    public double getPerimeter() {
        return perimeter;
	} // getPerimeter;

} // Shape (class)

