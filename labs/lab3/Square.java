public class Square extends Rect {

    Square(double width) {
        super(width, 0);
        width1 = width;
    } // Square(constructor)

    // Calculates the area of a square.
    public void calculateArea() {
        area = width1 * width1;
    } // calculateArea

    // Calculates the perimeter of a square.
    public void calculatePerimeter () {
        perimeter = 4 * width1;
    } // calculatePerimeter
} // Square (class)
