public class Oval extends Round {

    private double major2, minor2;

    // Oval constructor takes the variables from the Round class and sets them equal to the private doubles.
    Oval(double major, double minor) {
        major2 = major;
        minor2 = minor;
    } // Oval (constructor)

    // Calculates the area of an oval.
    public void calculateArea() {
        area = getPi()*major2*minor2;
    } // calculateArea

    // Calculates the perimeter of an oval.
    public void calculatePerimeter() {
        perimeter = 2 * getPi() * Math.sqrt(((major2*major2) + (minor2 * minor2)) / 2);
    } // calculatePerimeter

} // Oval (class)
