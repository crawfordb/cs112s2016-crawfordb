public class Circle extends Round {

    private double radius2;

    // Circle constructor takes the variables from the Round class and sets them equal to the private doubles.
    Circle(double radius) {
        radius2 = radius;
    } // Circle (constructor)

    // Calculates the area of a circle.
    public void calculateArea() {
        area = getPi() * (radius2 * radius2);
    } // calculateArea;

    // Calculates the perimeter of a circle.
    public void calculatePerimeter() {
        perimeter = 2*getPi()*radius2;
    } // calculatePerimeter

} // Circle (class)
