public class Quad extends Shape {

    protected double width1, height1;

    // Sets the width.
    public void setWidth(double width) {
        width1 = width;
    } // setWidth

    // Sets the height.
    public void setHeight(double height) {
        height1 = height;
    } // setHeight

    // Accessor to allow the Rect, Trapzd, and Square classes to use the variable.
    public double getWidth() {
        return width1;
    } // getWidth

    // Accessor to allow the Rect, Trapzd, and Square classes to use the variable.
    public double getHeight() {
        return height1;
    } // getHeight

} // Quad (class)
