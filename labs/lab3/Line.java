public class Line extends Shape {

    private double x3, y3, x4, y4;

    // Line constructor takes the variables from the ShapeTester class and sets them equal to the private doubles.
    Line(double x1, double y1, double x2, double y2) {
        x3 = x1;
        y3 = y1;
        x4 = x2;
        y4 = y2;
    } // Line (constructor)

    // Calculates the area of a line, but the area of a line is always 0.
    public void calculateArea() {
		area = 0;
    } // calculateArea

    // Calculates the perimeter of a line.
    public void calculatePerimeter() {
        perimeter = 2 * Math.sqrt(((x4 - x3) * (x4 - x3)) + (y4 - y3) * (y4 - y3));
    } // calculatePerimeter

} // Line (class)
