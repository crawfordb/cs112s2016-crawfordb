public class Trapzd extends Quad {

    private double otherBase1, side3, side4;

    // The Trapzd constructor takes the variables from the Quad class and initializes them.
    Trapzd(double width, double height, double otherBase, double side1, double side2) {
        width1 = width;
        height1 = height;
        otherBase1 = otherBase;
        side3 = side1;
        side4 = side2;
    } // Trapzd (constructor)

    // Calculates the area of a trapezoid.
    public void calculateArea() {
        area = height1 * ((width1 + otherBase1)/2);
    } // calculateArea

    // Calculates the perimeter of a trapezoid.
    public void calculatePerimeter() {
        perimeter = width1 + otherBase1 + side3 + side4;
    } // calculatePerimeter
} // Trapzd (class)
