import java.util.Scanner;

public class GuessMyNumber2 {

	public static void main(String [] args) {

        Scanner scan = new Scanner(System.in);

        int guess = 0;
        int userInput = 0;
        
        // Do-while loop runs through the game while the boolean playAgain is true. Once the game ends, checks whether playAgain is
        // true or false, and if it is false the game ends and the program stops running.

        do {

        	int max = 100;
        	int min = 1;
        	int count = 1;

            guess = (max + min) / 2;

            System.out.println("\nHello user! Please pick a number between 1 and 100.\n");

            System.out.println("My first guess is " + guess + ". How did I do?");
            userInput = scan.nextInt();

            // While the computer has not correctly guessed the number, the user inputs whether the computer's guess is too high (1) 
            // or too low (-1). The computer then outputs a new guess that is halfway between the min and max values. The program 
            // exits the while loop when the computer guesses the correct number. Also increments the count to keep track of the 
            // number of guesses.
            while(userInput != 0) {

                if(userInput == 1) {

                    max = guess;

                    guess = (max + min)/2;

                    System.out.println("\nHmmm, my guess was too high. My next guess is "+ guess +". How did I do?");

                } else if(userInput == -1) {

                    min = guess;

                    guess = (min + max)/2;

                    System.out.println("\nHmmm, my guess was too low. My next guess is "+ guess +". How did I do?");
                } // if-else

                userInput = scan.nextInt();
                count++;

            } // while

            System.out.println("\nYay, I guessed the correct number in "+ count +" tries! Your number is " + guess + ".\n");

        } while (playAgain());
    } // main

    // Checks for whether user wants to play game again or not. If not playAgain returns false and the program ends.
    public static boolean playAgain() {
        Scanner scan = new Scanner(System.in);

        System.out.println("Would you like me to guess again? (YES, NO)");
        String userInput = scan.next().toUpperCase();

        if (userInput.equals("YES")) {
            return true;
        } else if (userInput.equals("NO")) {
            return false;
        } // if-else
        return false;

    } // playAgain

} // class
