import java.util.Scanner;
import java.util.Random;

public class GuessMyNumber1 {

	public static void main(String[] args) {

		int guess = 0;
		int number = 0;

		Scanner scan = new Scanner(System.in);
		Random rand = new Random();

		// Do-while loop runs through the game while the boolean playAgain is true. Once the game ends, checks whether playAgain is 
		// true or false, and if it is false the game ends and the program stops running.
		do {

			int count = 1;
		
			System.out.println("\nHello user! I'm thinking of a number between 1 and 100. See if you can guess it!\n");
			System.out.println("Please enter a guess.");

			guess = scan.nextInt();

			number = rand.nextInt((100) + 1);

			// While the user has not correctly guessed the number, tells the user whether their guess is too high or too low and asks 
			// the user to guess again until the correct number is guessed. Also increments the count to keep track of the number of 
			// guesses entered.
			while(guess != number) {

				if(guess > number) {
					System.out.println("\nYour guess is too high! Guess again.");
					guess = scan.nextInt();
				} else if(guess < number) {
					System.out.println("\nYour guess is too low! Guess again.");
					guess = scan.nextInt();
				} // else
				count++;
			} // while

			System.out.println("\nYou got it in "+count+" tries. My number was "+number+".\n");

			} while (playAgain());
		} // main

	// Checks for whether user wants to play game again or not. If not playAgain becomes false and the program ends.
	public static boolean playAgain() {
		Scanner scan = new Scanner(System.in);

		System.out.println("Do you want to play again? (YES, NO)");
		String userInput = scan.next().toUpperCase();

		if (userInput.equals("YES")) {
			return true;
		} else if (userInput.equals("NO")) {
			return false;
		} // if-else
		return false;
	} // playAgain

} // class




