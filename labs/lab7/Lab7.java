import java.util.Stack;

public class Lab7 {

	public static void main(String[] args)
	{
    	System.out.println(convertToPostfix("(a + b)/(c + d)"));
	} // main

	public static boolean matcher(char[] c) {

		Stack<Character> myStack = new Stack<Character>();

		for (int i = 0; i < c.length; i++) {
			
			if (c[i] == '(' || c[i] == '[' || c[i] == '{') {
				myStack.push(c[i]);

			} else if (!myStack.empty()) {

				if (c[i] == ')') {
					char temp = myStack.pop();
					if (temp != '(') {
						return false;
					} //if
	
				} else if (c[i] == ']') {
					char temp = myStack.pop();
					if (temp != '[') {
						return false;
					} //if
	
				} else if (c[i] == '}') {
					char temp = myStack.pop();
					if (temp != '{') {
						return false;
					} //if

				} //if-else

			} else if (myStack.empty() && (c[i] == ')' || c[i] == ']' || c[i] == '}')) {
				return false;

			} //if-else

		} //for

		return myStack.empty();
	} //matcher

	private static boolean isOperator(char c)
	{
    	return c == '+' || c == '-' || c == '*' || c == '/' || c == '(' || c == ')';
	} // isOperator

	private static boolean isLowerPrecedence(char op1, char op2)
	{
    	switch (op1)
    	{
        	case '+':
        	case '-':
            	return !(op2 == '+' || op2 == '-');

        	case '*':
        	case '/':
            	return op2 == '^' || op2 == '(';

        	case '(':
            	return true;

        	default:
            	return false;
    	}
	} // isLowerPrecedence

	public static String convertToPostfix(String infix)
	{
    	Stack<Character> stack = new Stack<Character>();
    	StringBuffer postfix = new StringBuffer(infix.length());
    	char c;

    	for (int i = 0; i < infix.length(); i++)
    	{
        	c = infix.charAt(i);

        	if (!isOperator(c))
        	{
            	postfix.append(c);
        	} // if

        	else
        	{	
            	if (c == ')')
            	{
                	while (!stack.isEmpty() && stack.peek() != '(')
                	{
                    	postfix.append(stack.pop());
                	} // while
                	if (!stack.isEmpty())
                	{
                    	stack.pop();
                	} // if
            	} // if

            	else
            	{
                	if (!stack.isEmpty() && !isLowerPrecedence(c, stack.peek()))
                	{
                    	stack.push(c);
                	}
                	else
                	{
                    	while (!stack.isEmpty() && isLowerPrecedence(c, stack.peek()))
                    	{
                        	Character pop = stack.pop();
                        	if (c != '(')
                        	{
                            	postfix.append(pop);
                        	} else {
                          		c = pop;
                        	} // if else
                    	} // while
                    	stack.push(c);
                	} // if else

            	} // if else
        	} // if else
    	} // for
    	while (!stack.isEmpty()) {
      		postfix.append(stack.pop());
    	} // while
    	return postfix.toString();
	} // convertToPostfix

} // (class)
