class DoublyLinkedList {

	private static class Node {

		protected int data;
		protected Node next;
        protected Node previous;

		public Node() {
			next = null;
			data = 0;
            previous = null;
		} //Node (constructor)

		public Node(int d, Node n, Node p) {
			next = n;
			data = d;
            previous = p;
		} //Node (constructor)

	} //Node (class)



    // DoublyLinkedList stuff starts here
	private int size;
	private Node head, tail;

	public DoublyLinkedList() {
		head = null;
		tail = null;
		size = 0;
	} //DoublyLinkedList (constructor)

	public int size() {
		return size;
	} //size

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		} else {
			return false;
		} //if-else
	} //isEmpty

    public void add(int d) {

        Node newNode = new Node(d, null, null);

        if(size == 0) {
             head = newNode;
             tail = newNode;
             size++;
             return;

        } // if

        tail.next = newNode;
        
        newNode.previous = tail;
        tail = newNode;
        
        size++;
	} //add

	public int get(int index) {
		Node currentPosition = head;
		int counter = 0;

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- get");
			return -1;
		} //if

		while (counter < index/* && index >= 0 && index < size*/) {
			currentPosition = currentPosition.next;
			counter++;
		} //while

		return currentPosition.data;
	} //get

	public int getFromEnd(int index) {
        Node currentPosition = tail;
        int counter = 0;

        // check to make sure the index is less than size
		if (index >= size || index < 0) {
		    System.out.println("Hey you gave me bad input -- get");
			return -1;
		} //if

        while(counter < index) {
            currentPosition = currentPosition.previous;
            counter++;
        } // whule

        return currentPosition.data;

	} //getFromEnd

	public void remove(int index) {

        if(index >= size || index < 0) {
            System.out.println("Hey you gave me bad input -- remove");
            return;
        } // if

        // 1 - removing head
        if(index == 0) {
            head = head.next;

            if(head != null) {
             	 
             	head.previous = null;
            } // if

             // 3 - removing single node
             if(size == 1) {
                 tail = null;
             } // if
        } else {
            // 4 - removing a regular node
            Node currentLocation, previousLocation;
            currentLocation = head.next;
            previousLocation = head;
            int counter = 1;

            while (counter < index) {
                currentLocation = currentLocation.next;
                previousLocation = previousLocation.next;
                counter++;
            } // while

            previousLocation.next = currentLocation.next;
            
            if(currentLocation != null || currentLocation.next != null) {

            	currentLocation.next.previous = previousLocation;
            } // if

            // 2 - removing tail
            if(index == size-1) {
                tail = previousLocation;
            } // if
        } // if-else

        size--;
	} //remove

	public boolean testPreviousLinks() {
		Node current = tail;
		int count = 1;
		while (current.previous != null) {
			current = current.previous;
			count++;
		} //while
		return ((current == head) && (count == size));
	} //testPreviousLinks

} //DoublyLinkedList (class)
