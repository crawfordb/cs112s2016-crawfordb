class DoublyLinkedList {

	private static class Node {

		protected int data;
		protected Node next;

		public Node() {
			next = null;
			data = 0;
		} //Node (constructor)

		public Node(int d, Node n) {
			next = n;
			data = d;
		} //Node (constructor)

	} //Node (class)



    // DoublyLinkedList stuff starts here
	private int size;
	private Node head, tail;

	public DoublyLinkedList() {
		head = null;
		tail = null;
		size = 0;
	} //DoublyLinkedList (constructor)

	public int size() {
		return size;
	} //size

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		} else {
			return false;
		} //if-else
	} //isEmpty

	public void add(int newInt) {





	} //add

	public int get(int index) {
		Node currentPosition = head;
		int counter = 0;

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- get");
			return -1;
		} //if

		while (counter < index/* && index >= 0 && index < size*/) {
			currentPosition = currentPosition.next;
			counter++;
		} //while

		return currentPosition.data;
	} //get

	public int getFromEnd(int index) {





	} //getFromEnd

	public void remove(int index) {





	} //remove

	public boolean testPreviousLinks() {
		Node current = tail;
		int count = 1;
		while (current.previous != null) {
			current = current.previous;
			count++;
		} //while
		return ((current == head) && (count == size));
	} //testPreviousLinks

} //DoublyLinkedList (class)
