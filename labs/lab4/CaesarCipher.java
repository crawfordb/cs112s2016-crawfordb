public class CaesarCipher {

    public static void main(String[] args) {

        String message = "HELLO USER";
        char[] a = message.toCharArray();
        int shift = 3;
        int digit = 0;
        String encrypted = "";

		for (int count = 0; count < a.length; count++) {
            digit = a[count];
		} // for
        
        System.out.println(a);

		encrypt(message, shift, encrypted);
		decrypt(message, shift, encrypted);

    } // main

    public static void encrypt(String message, int shift, String encrypted) {

    	int encryption = 0;
    	

        for(int i = 0; i < message.length(); i++) {

        	if(message.charAt(i) != ' ') {

            encryption = message.charAt(i) + shift;
            encrypted += (char)encryption;

			} else if (message.charAt(i) == ' ') {
				
				encrypted += ' ';

			} // if
        } // for

		System.out.println(encrypted);

		decrypt(message, shift, encrypted);

    } // encrypt()

    public static void decrypt(String message, int shift, String encrypted) {

    	int encryption = 0;
    	String encrypted2 = "";

        for(int i = 0; i < encrypted.length(); i++) {

        	if(message.charAt(i) != ' ') {

            	encryption = encrypted.charAt(i) - shift;
            	encrypted2 += (char)encryption;

			} else if (message.charAt(i) == ' ') {
				
				encrypted2 += ' ';

			} // if
        } // for

        System.out.println(encrypted2);

    } // decrypt()
} // CaesarCipher (class)


