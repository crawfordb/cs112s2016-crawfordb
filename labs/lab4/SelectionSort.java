import java.util.Arrays;

public class SelectionSort {

	public static void main(String[] args) {

		int[] n = {8, 3, 4, 2, 1};

		for(int i = 0; i < n.length - 1; i++) {

            int j = 0;
            int index = i;
            int smallerNumber = 0;

		    for(j = i + 1; j < n.length; j++) {

                if(n[j] < n[index]) {
                   index = j;
                } // if

			} // for loop

            smallerNumber = n[index];
            n[index] = n[i];
            n[i] = smallerNumber;

            System.out.println(Arrays.toString(n));

		} // for loop

	} // main

} // SelectionSort (class)

