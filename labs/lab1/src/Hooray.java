public class Hooray {

    //This is the hooray method. It is void which means that it does not return anything.
    public static void hooray() {

		//This is a while loop. It continues to print out "Hooray!" while it is true.
		while(true) {
			System.out.println("Hooray!");
    	} //while

    } //hooray

    //This is the main method. The hooray method is called here.
    public static void main(String[] args) {
		hooray();
    } //main

} //Hooray (class)
