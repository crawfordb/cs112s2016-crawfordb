public class Weeee {

    //This is the weeee method. It is void which means that it does not return anything. 
    public static void weeee() {
		System.out.println("Weeee!"); //This line prints out "Weeee!"
		weeee(); //This line calls the method.
    } //weeee

    //This is the main method. The weeee is called again within this method.
    public static void main(String[] args) {
		weeee();
    } //main

} //Weeee (class)
